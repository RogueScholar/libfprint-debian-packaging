-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA512

Format: 3.0 (quilt)
Source: libfprint
Binary: libfprint-dev, libfprint0, libfprint-doc
Architecture: any all
Version: 1:0.99.0-0.1
Maintainer: FingerForce Team <fingerforce-devel@lists.alioth.debian.org>
Uploaders: Ulises Vitulli <dererk@debian.org>, Didier Raboud <odyx@debian.org>
Homepage: https://fprint.freedesktop.org/
Standards-Version: 4.3.0
Vcs-Browser: https://gitlab.freedesktop.org/libfprint/libfprint
Vcs-Git: https://gitlab.freedesktop.org/libfprint/libfprint.git
Build-Depends: debhelper-compat (= 11), libusb-1.0-0-dev (>= 2:1.0.20), libglib2.0-dev (>= 2.48), libpixman-1-dev (>= 0.33.6), libxv-dev (>= 2:1.0.10), libnss3-dev (>= 2:3.21), libgtk-3-dev (>= 3.22.30), gtk-doc-tools (>= 1.25), meson (>= 0.45.0), libudev-dev (>= 237)
Package-List:
 libfprint-dev deb libdevel optional arch=any
 libfprint-doc deb doc optional arch=all
 libfprint0 deb libs optional arch=any
Checksums-Sha1:
 1644488ab2095384beaee2b37c657c0ae3f78ecd 332928 libfprint_0.99.0.orig.tar.xz
 6ad6ca29af9157fd6b17a6c4c82f33ce4b4bb0bd 9084 libfprint_0.99.0-0.1.debian.tar.xz
Checksums-Sha256:
 0785974bc5d534492fde1e2e2a44a7cbaf8334f9ade2284f8959020f08b5249b 332928 libfprint_0.99.0.orig.tar.xz
 f07b785652770a2710b8390c3292047fa8e7ca5ad2145ea0d93b0b8f18cadd9c 9084 libfprint_0.99.0-0.1.debian.tar.xz
Files:
 15941b3aeb6b1fe1225064ce07a6f722 332928 libfprint_0.99.0.orig.tar.xz
 782ba76b91b5221af5a4ccaaabc731c0 9084 libfprint_0.99.0-0.1.debian.tar.xz

-----BEGIN PGP SIGNATURE-----

iQGzBAEBCgAdFiEER2xYiOw5heh6Ewa0pnlQNOli/KsFAlzisnAACgkQpnlQNOli
/KtiyQv+NrU0dJHs35zZsj6LzL/N711zv5LQIrh4bfJbhAAj+N/SGC6Z4rQA3KnZ
scARsSFlzXhgaiASFzvApwW0Xiq8I7X7uAUKw642YnOF2nrAEBugKnAvr8WT6qMP
q9J27QRBeTmBXMrMGZZNLG4CFvLgBoprjKpE7Uw0SVVgkHGeEIyaX3+C7JIhDUqq
TOjUk+lA7AsbfQ5qWWyJs+TbtpDRO0ke0SdJb2NlOKxC94neEvVuyOoulUL/Xdk6
3nAhvNgTCUFP8wVlYJ+lNc97pMJ20QAebxxJhVFb8ZZKTSHt9NsRNWxiYfA440u6
oaanEycyH7Wr67QCtML2npvgEhL0Fxb7YjYC4LaYFXZtGRAualKxAIXaS2PHQWpb
bdZvgUgFduPBzuAfG9CcoaqSRFzCdWgHGZSSgkwgvLMRLA4nKnV6/k+WHrGNZnBn
ITlA5qGFZ5KEZHfQkZBrUwubN4s2f02Rb6J5b9lNAn20wYTqgSJifTq1PleyL6mk
FDmAJqNS
=+7an
-----END PGP SIGNATURE-----
